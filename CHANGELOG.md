# Changelog
## 0.2 - 2018-02-13
### Added
- CHANGELOG.md

### Changed
- README.md
- Start versioning based on the 0.2

## 0.2.1 - 2018-02-13
### Changed
- Move META description out of title




