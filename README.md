# qrgen.me

Code that powers [qrgen.me](https://qrgen.me) 


## Getting started

### Installing

Install modules  
```
npm run install
```

### Development
Run webpack-dev-server
```
npm run start
```

## Build
Clean `dist` then create and populate, build production assets inside it  
```
npm run build && npm run favicons
```

### Deployment
Deploy with
```
git push live master
```
## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details.


