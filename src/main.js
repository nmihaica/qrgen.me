import css from './style.scss'
import Vue from 'vue';

import vueQR from 'qrcode.vue';

let vm = new Vue({
    el: '#app',
    components: {
        qrCode: vueQR,
    },
    data() {
        return {
            rawHtml: ``,
            message: '',
            size: 432,
            background: '#FFFFFF',
            foreground: '#000000',
            imgUrl: ''
        };
    },
    methods: {
        downloadImage: function () {
            const canvas = document.getElementsByTagName("canvas")[0];
            const imgEncoded = canvas.toDataURL("image/png");
            const image = '<img id="generatedImage" src="' + imgEncoded + '"/>'
            this.imgUrl = imgEncoded
            this.rawHtml = `
                <div>
                    <h1 class="title">Generated image:</h1>`
                    + image + `
                </div>
            `
            
        },
        resetData: function () {
            this.message = '',
                this.size = 432,
                this.background = '#FFFFFF',
                this.foreground = '#000000'
        },
        resetImage: function () {
            this.rawHtml = '';
        },
/*         forceBASE64Download: function(){
            console.log("FORCING BASE 64")
            let img = document.getElementById('generatedImage');
            console.log("bseaIMG: ",img)
            img.onclick = function() {
                // atob to base64_decode the data-URI
                let image_data = atob(img.src.split(',')[1]);
                // Use typed arrays to convert the binary data to a Blob
                let arraybuffer = new ArrayBuffer(image_data.length);
                let view = new Uint8Array(arraybuffer);
                for (let i=0; i<image_data.length; i++) {
                    view[i] = image_data.charCodeAt(i) & 0xff;
                }
                try {
                    // This is the recommended method:
                    let blob = new Blob([arraybuffer], {type: 'application/octet-stream'});
                } catch (e) {
                    // The BlobBuilder API has been deprecated in favour of Blob, but older
                    // browsers don't know about the Blob constructor
                    // IE10 also supports BlobBuilder, but since the `Blob` constructor
                    //  also works, there's no need to add `MSBlobBuilder`.
                    let bb = new (window.WebKitBlobBuilder || window.MozBlobBuilder);
                    bb.append(arraybuffer);
                    let blob = bb.getBlob('application/octet-stream'); // <-- Here's the Blob
                }
            
                // Use the URL object to create a temporary URL
                let url = (window.webkitURL || window.URL).createObjectURL(blob);
                location.href = url; // <-- Download!
            };
        } */
    },
})